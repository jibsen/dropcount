
#ifndef RSRC_H_INCLUDED
#define RSRC_H_INCLUDED

#define IDI_MAINICON         500

#define IDD_MAIN             2000
#define IDD_ABOUT            2001

#define IDC_ABOUT            2500
#define IDC_RESET            2501
#define IDC_QUIT             2502
#define IDC_ONTOP            2503
#define IDC_LINK             2504
#define IDC_FCOUNT           2505
#define IDC_DCOUNT           2506
#define IDC_TSIZE            2507
#define IDC_MPATH            2508
#define IDC_STATUS           2509
#define IDC_DOCMDLINE        2510

#endif /* RSRC_H_INCLUDED */
