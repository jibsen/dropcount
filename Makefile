##
## DropCount - count files and find max depth
##
## DropCount makefile (NMAKE)
##
## Copyright 2005-2014 Joergen Ibsen
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
## http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

target	= DropCount.exe
objs	= DropCount.obj
ress	= rsrc.res

cflags	= /nologo /c /W2 /O1 /Os /I\dev\wcrt\include
cvars	=
lflags	= /nologo /subsystem:windows,5.0
llibs	= \dev\wcrt\lib\wcrts.lib

all: $(target)

.c.obj:
	$(CC) $(cdebug) $(cflags) $(cvars) $<

.rc.res:
	$(RC) $(rcflags) $(rcvars) $<

$(target): $(objs) $(ress)
	link $(ldebug) $(lflags) /out:$@ $** $(llibs)

clean:
	del /Q $(objs) $(ress) $(target)
