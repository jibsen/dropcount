
DropCount
=========

Copyright 2005-2014 Joergen Ibsen

<http://www.ibsensoftware.com/>


About
-----

DropCount is a small utility that that you can drag and drop folders onto
to get a report of file count, folder count, total size, presence of
hidden/readonly subdirs, and max path length (useful for cd burning).

I wrote it as a coding snack for [DonationCoder][].

[DonationCoder]: http://www.donationcoder.com/
