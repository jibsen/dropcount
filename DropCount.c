/*
 * DropCount - count files and find max depth
 *
 * Copyright 2005-2014 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <windows.h>
#include <commctrl.h>

#include "rsrc.h"

/* a little WCRT magic */
extern unsigned int wcrt_parse_argv(const char *cl, int *argc, char **argv,
                                    char *buffer);

#ifdef _MSC_VER
/* include win32api libraries */
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"shell32.lib")
#pragma comment(lib,"comctl32.lib")
#endif

#ifdef _MSC_VER
typedef unsigned __int64 uint64;
#else
typedef unsigned long long uint64;
#endif

HINSTANCE hInst;

CRITICAL_SECTION cs;

WIN32_FILE_ATTRIBUTE_DATA fad;
static char path[4096];

HWND hDialog;
HWND hWndTT;
HINSTANCE hInst;

unsigned long num_files;
unsigned long num_fh, num_fs, num_fr;
unsigned long num_dirs;
unsigned long num_dh, num_ds, num_dr;
unsigned long max_path;
unsigned long max_rpath;
unsigned long max_depth;
unsigned long max_rdepth;
unsigned long num_error;
uint64 total_size;

unsigned long rdepth;
unsigned long cur_depth;
unsigned long cur_pathlen;

static char tsep[5] = ",";
static char dsep[5] = ".";

int argc;
char **argv;

static void reset_counts(void)
{
	num_files = 0;
	num_fh = num_fs = num_fr = 0;
	num_dirs = 0;
	num_dh = num_ds = num_dr = 0;
	max_path = max_rpath = 0;
	max_depth = max_rdepth = 0;
	total_size = 0;

	rdepth = 0;
	cur_depth = 0;
	cur_pathlen = 0;
}

BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam,
                           LPARAM lParam)
{
	switch (message) {
	case WM_INITDIALOG:
		return TRUE;

	case WM_CLOSE:
		PostMessage(hDlg, WM_COMMAND, IDOK, 0);
		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case IDOK:
		case IDCANCEL:
			EndDialog(hDlg, 0);
			return TRUE;

		case IDC_LINK:
			ShellExecute(NULL, "open", "http://www.donationcoder.com/", NULL,
			             NULL, 0);
			return TRUE;

		default:
			return FALSE;
		}

	default:
		return FALSE;
	}

	return TRUE;
}

static void format_int(char *p, uint64 v)
{
	size_t len, i;
	uint64 vv;

	vv = v;
	len = 0;

	do {
		unsigned int digit;

		if ((len & 3) == 3) {
			p[len++] = tsep[0];
		}

		digit = (unsigned int) (vv % 10u);

		p[len++] = '0' + digit;

		vv /= 10u;
	} while (vv);

	p[len--] = 0;

	for (i = 0; i < len; ++i, --len) {
		char t;

		t = p[i];
		p[i] = p[len];
		p[len] = t;
	}
}

static void update_counts(HWND hDlg)
{
	char buffer[128];
	unsigned int fp, ip;

	format_int(buffer, num_files);
	if (num_fr || num_fs || num_fh) {
		wsprintf(buffer + strlen(
		             buffer), "  (%s%s%s)", num_fr ? "R" : "", num_fs ? "S" : "",
		         num_fh ? "H" : "");
	}
	SetDlgItemText(hDlg, IDC_FCOUNT, buffer);

	format_int(buffer, num_dirs);
	wsprintf(buffer + strlen(buffer), "  (");
	if (num_fr || num_fs || num_fh) {
		wsprintf(buffer + strlen(
		             buffer), "%s%s%s, ", num_fr ? "R" : "", num_fs ? "S" : "",
		         num_fh ? "H" : "");
	}
	wsprintf(buffer + strlen(buffer), "depth %lu / %lu)", max_rdepth,
	         max_depth);
	SetDlgItemText(hDlg, IDC_DCOUNT, buffer);

	format_int(buffer, total_size);
	strcat(buffer, " bytes");

	if (total_size >= ((uint64) 1000) * 1000 * 1000 * 1000u) {
		ip = (unsigned int) (total_size / (1024 * 1024 * 1024u));
		fp = (100 * ip) / 1024u;
		ip /= 1024u;
		fp %= 100u;
		wsprintf(buffer + strlen(buffer), "  (%u%c%02u TB)", ip, dsep[0], fp);
	}
	else if (total_size >= 1000 * 1000 * 1000u) {
		ip = (unsigned int) (total_size / (1024 * 1024u));
		fp = (100 * ip) / 1024u;
		ip /= 1024u;
		fp %= 100u;
		wsprintf(buffer + strlen(buffer), "  (%u%c%02u GB)", ip, dsep[0], fp);
	}
	else if (total_size >= 1000 * 1000u) {
		ip = (unsigned int) total_size / 1024u;
		fp = (100 * ip) / 1024u;
		ip /= 1024u;
		fp %= 100u;
		wsprintf(buffer + strlen(buffer), "  (%u%c%02u MB)", ip, dsep[0], fp);
	}
	else {
		ip = (unsigned int) total_size / 1024u;
		fp = (100 * (unsigned int) total_size) / 1024u;
		fp %= 100u;
		wsprintf(buffer + strlen(buffer), "  (%u%c%02u KB)", ip, dsep[0], fp);
	}

	SetDlgItemText(hDlg, IDC_TSIZE, buffer);

	wsprintf(buffer, "%lu / %lu", max_rpath, max_path);
	SetDlgItemText(hDlg, IDC_MPATH, buffer);
}

static void update_status(void)
{
	unsigned int plen = strlen(path + cur_pathlen);

	if (plen < 54) {
		SetDlgItemText(hDialog, IDC_STATUS, path + cur_pathlen);
	}
	else {
		char buf[54];
		unsigned int i;

		for (i = 0; i < 25; ++i) {
			buf[i] = path[cur_pathlen + i];
		}
		buf[25] = '.';
		buf[26] = '.';
		buf[27] = '.';
		for (i = 0; i < 25; ++i) {
			buf[28 + i] = path[cur_pathlen + plen - 50 + i];
		}
		buf[53] = 0;

		SetDlgItemText(hDialog, IDC_STATUS, buf);
	}
}

static void process_folder(void)
{
	WIN32_FIND_DATA fd;
	HANDLE hFind;
	DWORD dwError;
	DWORD pathlen;

	++num_dirs;

	++rdepth;

	update_status();

	/* update max depth */
	if (rdepth > max_rdepth) {
		max_rdepth = rdepth;
	}
	if (cur_depth + rdepth > max_depth) {
		max_depth = cur_depth + rdepth;
	}

	pathlen = strlen(path);

	if (pathlen > max_path) {
		max_path = pathlen;
	}
	if (pathlen - cur_pathlen > max_rpath) {
		max_rpath = pathlen - cur_pathlen;
	}

	/* add backslash if not present */
	if (pathlen && path[pathlen - 1] != '\\') {
		path[pathlen++] = '\\';
	}

	/* add '*' */
	path[pathlen] = '*';
	path[pathlen + 1] = 0;

	hFind = FindFirstFile(path, &fd);

	/* strip '*' */
	path[pathlen] = 0;

	if (hFind == INVALID_HANDLE_VALUE) {
		--rdepth;
		++num_error;
		return;
	}

	do {
		/* add filename */
		strcpy(path + pathlen, fd.cFileName);

		if (GetFileAttributesEx(path, GetFileExInfoStandard, &fad)) {
			/* skip '.' and '..' */
			if (!strcmp(fd.cFileName, ".") || !strcmp(fd.cFileName, "..")) {
				continue;
			}

			if (fad.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
					++num_dh;
				}
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
					++num_ds;
				}
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
					++num_dr;
				}

				process_folder();
			}
			else {
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
					++num_fh;
				}
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
					++num_fs;
				}
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
					++num_fr;
				}

				/* update max path length */
				{
					DWORD flen = strlen(fd.cFileName);

					if (pathlen + flen > max_path) {
						max_path = pathlen + flen;
					}
					if (pathlen - cur_pathlen + flen > max_rpath) {
						max_rpath = pathlen - cur_pathlen + flen;
					}
				}

				++num_files;
				total_size +=
				    (((uint64) fad.nFileSizeHigh) << 32) + fad.nFileSizeLow;
			}
		}
		else {
			++num_error;
		}
	} while (FindNextFile(hFind, &fd));

	--rdepth;

	/* strip filename */
	path[pathlen] = 0;

	dwError = GetLastError();

	FindClose(hFind);

	if (dwError != ERROR_NO_MORE_FILES) {
		++num_error;
		return;
	}
}

DWORD WINAPI process_drop(HDROP hDrop)
{
	UINT num;
	UINT i;

	EnterCriticalSection(&cs);

	EnableWindow(GetDlgItem(hDialog, IDC_RESET), FALSE);

	rdepth = 0;

	num = DragQueryFile(hDrop, 0xFFFFFFFF, NULL, 0);

	for (i = 0; i < num; ++i) {
		if (DragQueryFile(hDrop, i, path, sizeof(path))) {
			cur_pathlen = strlen(path);

			{
				DWORD j;

				cur_depth = 0;

				for (j = 0; j < cur_pathlen; ++j) {
					if (path[j] == '\\') {
						++cur_depth;
					}
				}
			}

			/* update max path length */
			if (cur_pathlen > max_path) {
				max_path = cur_pathlen;
			}

			if (GetFileAttributesEx(path, GetFileExInfoStandard, &fad)) {
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
						++num_dh;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
						++num_ds;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
						++num_dr;
					}

					process_folder();
				}
				else {
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
						++num_fh;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
						++num_fs;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
						++num_fr;
					}

					++num_files;
					total_size +=
					    (((uint64) fad.nFileSizeHigh)
					<< 32) + fad.nFileSizeLow;
				}
			}
			else {
				++num_error;
			}
		}
	}

	DragFinish(hDrop);

	SetDlgItemText(hDialog, IDC_STATUS, "Ready");

	update_counts(hDialog);

	EnableWindow(GetDlgItem(hDialog, IDC_RESET), TRUE);

	LeaveCriticalSection(&cs);

	return 0;
}

DWORD WINAPI process_commandline(void *not_used)
{
	UINT num;
	UINT i;

	(void) not_used;

	EnterCriticalSection(&cs);

	EnableWindow(GetDlgItem(hDialog, IDC_RESET), FALSE);

	rdepth = 0;

	num = argc;

	for (i = 1; i < num; ++i) {
		lstrcpyn(path, argv[i], sizeof(path) / sizeof(path[0]) - 1);
		path[sizeof(path) / sizeof(path[0]) - 1] = 0;

		{
			cur_pathlen = strlen(path);

			{
				DWORD j;

				cur_depth = 0;

				for (j = 0; j < cur_pathlen; ++j) {
					if (path[j] == '\\') {
						++cur_depth;
					}
				}
			}

			/* update max path length */
			if (cur_pathlen > max_path) {
				max_path = cur_pathlen;
			}

			if (GetFileAttributesEx(path, GetFileExInfoStandard, &fad)) {
				if (fad.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
						++num_dh;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
						++num_ds;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
						++num_dr;
					}

					process_folder();
				}
				else {
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
						++num_fh;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) {
						++num_fs;
					}
					if (fad.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
						++num_fr;
					}

					++num_files;
					total_size +=
					    (((uint64) fad.nFileSizeHigh)
					<< 32) + fad.nFileSizeLow;
				}
			}
			else {
				++num_error;
			}
		}
	}

	SetDlgItemText(hDialog, IDC_STATUS, "Ready");

	update_counts(hDialog);

	EnableWindow(GetDlgItem(hDialog, IDC_RESET), TRUE);

	LeaveCriticalSection(&cs);

	return 0;
}

static void add_tooltip(DWORD id, char *tip)
{
	TOOLINFO ti;

	ti.cbSize = sizeof(ti);
	ti.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
	ti.hwnd = hDialog;
	ti.hinst = hInst;
	ti.uId = (UINT) GetDlgItem(hDialog, id);
	ti.lpszText = tip;

	SendMessage(hWndTT, TTM_ADDTOOL, 0, (LPARAM) &ti);
}

BOOL CALLBACK MainDlgProc(HWND hDlg, UINT message, WPARAM wParam,
                          LPARAM lParam)
{
	switch (message) {
	case WM_INITDIALOG:
		hDialog = hDlg;

		/* add tooltips */
		add_tooltip(IDC_FCOUNT, "R = Read-Only, S = System, H = Hidden");
		add_tooltip(IDC_DCOUNT,
		            "Depth is max relative / absolute folder nesting depth");
		add_tooltip(IDC_MPATH,
		            "Max relative / absolute path length in characters");
		add_tooltip(IDC_ONTOP, "Keep DragCount window on top");
		add_tooltip(IDC_RESET, "Reset all counts");

		/* set stay on top */
		CheckDlgButton(hDlg, IDC_ONTOP, BST_CHECKED);
		SetWindowPos(hDlg, HWND_TOPMOST, 0, 0, 0, 0,
		             SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

		/* init status display */
		update_counts(hDlg);

		if (argc > 1) {
			DWORD dwThreadID;
			CreateThread(NULL, 0, process_commandline, 0, 0, &dwThreadID);
		}

		/* accept drag and drop files */
		DragAcceptFiles(hDlg, TRUE);
		return TRUE;

	case WM_CLOSE:
		PostMessage(hDlg, WM_COMMAND, IDC_QUIT, 0);
		return TRUE;

	case WM_DROPFILES:
		{
			DWORD dwThreadID;
			CreateThread(NULL, 0, process_drop, (LPVOID) wParam, 0,
			             &dwThreadID);
		}
		return 0;

	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case IDC_RESET:
			reset_counts();
			update_counts(hDlg);
			return TRUE;

		case IDC_ONTOP:
			/* toggle on top */
			if (IsDlgButtonChecked(hDlg, IDC_ONTOP) == BST_CHECKED) {
				SetWindowPos(hDlg, HWND_TOPMOST, 0, 0, 0, 0,
				             SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
			}
			else {
				SetWindowPos(hDlg, HWND_NOTOPMOST, 0, 0, 0, 0,
				             SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
			}
			return TRUE;

		case IDC_ABOUT:
			DialogBoxParam(hInst, MAKEINTRESOURCE(
			                   IDD_ABOUT), hDlg, AboutDlgProc, 0);
			return TRUE;

		case IDCANCEL:
		case IDC_QUIT:
			EndDialog(hDlg, 0);
			return TRUE;

		default:
			return FALSE;
		}

	default:
		return FALSE;
	}

	return TRUE;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
{
	char *clbuffer = NULL;

	hInst = hInstance;

	/* init common controls */
	{
		INITCOMMONCONTROLSEX icex;

		icex.dwSize = sizeof(icex);
		icex.dwICC = ICC_BAR_CLASSES;

		if (!InitCommonControlsEx(&icex)) {
			return 1;
		}
	}

	/* create the ToolTip control */
	hWndTT = CreateWindowEx(0, TOOLTIPS_CLASS, NULL, TTS_ALWAYSTIP,
	                        CW_USEDEFAULT, CW_USEDEFAULT,
	                        CW_USEDEFAULT, CW_USEDEFAULT,
	                        NULL, (HMENU) NULL, hInstance,
	                        NULL);

	SetWindowPos(hWndTT, HWND_TOPMOST, 0, 0, 0, 0,
	             SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	/* parse command line into argv array */
	{
		char *cl;
		int clsize;

		cl = GetCommandLine();

		clsize = wcrt_parse_argv(cl, &argc, NULL, NULL);

		clbuffer = (char *) malloc(clsize);

		if (clbuffer) {
			wcrt_parse_argv(
			    cl,
			    &argc,
			    (char **) clbuffer,
			    clbuffer + (argc + 1) * sizeof(char *)
			    );
		}
		else {
			argc = 0;
		}

		argv = (char **) clbuffer;
	}

	reset_counts();

	/* get thousands separator */
	if (GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, tsep,
	                  sizeof(tsep) / sizeof(tsep[0])) != 2) {
		tsep[0] = ',';
		tsep[1] = 0;
	}

	/* get decimal separator */
	if (GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, dsep,
	                  sizeof(dsep) / sizeof(dsep[0])) != 2) {
		dsep[0] = '.';
		dsep[1] = 0;
	}

	InitializeCriticalSection(&cs);

	hInst = hInstance;

	DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, MainDlgProc, 0);

	DeleteCriticalSection(&cs);

	if (clbuffer) {
		free(clbuffer);
	}

	return 0;
}
